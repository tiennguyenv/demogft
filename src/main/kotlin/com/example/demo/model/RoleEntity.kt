package com.example.demo.model

import javax.persistence.*

@Entity
@Table(name= "ROLES")
data class RoleEntity (
        @Id
        @Column(name="id")
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = -1,

        @Column(name= "ROLE")
        var role: String
)