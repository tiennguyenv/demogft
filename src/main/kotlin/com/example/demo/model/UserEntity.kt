package com.example.demo.model

import javax.persistence.*

@Entity
@Table(name= "USERS")
data class UserEntity (

        @Column(name= "USERNAME")
        var username: String,

        @Column(name= "PASSWORD")
        var password: String,

        @OneToMany(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
        @JoinTable(name= "USERS_ROLES", joinColumns = [JoinColumn(name="USER_ID")],
        inverseJoinColumns = [JoinColumn(name="ROLE_ID")])
        var roles: Set<RoleEntity>?,

        @Id
        @Column(name= "ID")
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = -1
) {
        public constructor() : this("", "", null )

}
