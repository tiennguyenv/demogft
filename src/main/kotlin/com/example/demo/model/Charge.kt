package com.example.demo.model

import javax.persistence.*

@Entity
@Table(name= "CHARGE")
data class Charge (

        @Id
        var CARD_ID: String?,

        @Column(name = "CARDNO")
        var cardNumber: String,

        @Column(name = "CVV")
        var cvv: String,

        @Column(name = "EXPIRY_DATE")
        var expirydate: String,

        @Column(name = "AMOUNT")
        var amount: Int,

        @Column(name = "CURRENCY")
        var currency: String = "",

        @Column(name = "STATUS")
        var status: String = ""

)