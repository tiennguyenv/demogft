package com.example.demo.jwts

import java.io.Serializable


class JwtResponse : Serializable {
    lateinit var accessToken: String
    lateinit var username: String
    var roles: List<String>
        private set
    var id : Long = -1

    constructor(accessToken: String?, username: String?, roles: MutableList<String>, id: Long) {
        if (accessToken != null) {
            this.accessToken = accessToken
        }
        if (username != null) {
            this.username = username
        }
        this.roles = roles
            this.id = id
    }

    constructor(token: String, username: String, roles: List<String>) {
        accessToken = token
        this.username = username
        this.roles = roles
    }

    companion object {
        private const val serialVersionUID = -8091879091924046844L
    }
}
