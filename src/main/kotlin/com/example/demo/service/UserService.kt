package com.example.demo.service

import com.example.demo.model.UserEntity
import com.example.demo.repository.UserRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class UserService (
        private val userRepository: UserRepository
) {
    private val logger: Logger = LoggerFactory.getLogger(UserService::class.java)
    fun findAllUsers(): List<UserEntity> {
        return userRepository.findAll().toList()
    }
}