package com.example.demo.service

import com.example.demo.model.UserEntity
import com.example.demo.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class CustomUserDetailsService(userRepository: UserRepository) : UserDetailsService {
    @Autowired
    lateinit var  userRepository: UserRepository
    override fun loadUserByUsername(username: String): UserDetails {
        val user: UserEntity = userRepository.findByUsername(username)
        var userDetails: CustomUserDetails?
        if(user != null) {
            userDetails = CustomUserDetails(user)
        } else {
            throw UsernameNotFoundException("User not exist with name : $username")
        }
        return userDetails
    }
}