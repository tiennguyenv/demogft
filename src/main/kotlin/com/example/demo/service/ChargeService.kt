package com.example.demo.service

import com.example.demo.model.Charge
import com.example.demo.repository.ChargeRepository
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Service
import java.util.*


@Service
class ChargeService (
    private val kafkaTemplate: KafkaTemplate<String, Charge>,
    private val chargeRepository: ChargeRepository
) {
    private val logger: Logger = LoggerFactory.getLogger(ChargeService::class.java)

    @Value("\${kafka.users-topic}")
    private lateinit var topic: String

    fun createCharge(charge: Charge): Charge {
        val uuid = UUID.randomUUID()
        charge.CARD_ID = uuid.toString()
        charge.status = Companion.SUCCESS_STATUS
        kafkaTemplate.send(topic, charge)
        logger.info("Sent to Kafka Message: " + ObjectMapper().writeValueAsString(charge))
        return charge
    }

    @KafkaListener(topics = ["\${kafka.users-topic}"], groupId = "\${kafka.group-id}", containerFactory = "kafkaListenerContainerFactory")
    private fun consumeCharge (charge: Charge) {
        try {
            logger.info("Consumed JSON Message: ${ObjectMapper().writeValueAsBytes(charge)}")
            val chargePut = Charge(charge.CARD_ID, charge.cardNumber, charge.cvv, charge.expirydate ,charge.amount, charge.currency, charge.status)
            val saved = chargeRepository.save(chargePut)
            logger.info("saved complete:  $saved")
        } catch (e: JsonProcessingException) {
            // Not to-do
        }
    }

    fun findAll(): List<Charge> {
        return chargeRepository.findAll().toList()
    }
    companion object {
        private const val SUCCESS_STATUS: String = "success"
    }
}