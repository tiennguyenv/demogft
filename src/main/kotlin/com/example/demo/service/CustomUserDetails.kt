package com.example.demo.service

import com.example.demo.model.RoleEntity
import com.example.demo.model.UserEntity
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.stream.Collectors

class CustomUserDetails(user: UserEntity) : UserDetails {
    private var user: UserEntity
    fun getUser(): UserEntity {
        return user
    }

    fun setUser(user: UserEntity) {
        this.user = user
    }

    override fun getAuthorities(): Collection<GrantedAuthority> {
        val grantList: MutableList<GrantedAuthority> = ArrayList()
        val authorities: List<GrantedAuthority> = user.roles!!.stream().map { role: RoleEntity ->
            SimpleGrantedAuthority(role.role)}.collect(Collectors.toList())
        for (authority in authorities) {
            grantList.add(authority)
        }
        return grantList
    }
    override fun getPassword(): String {
        return user.password
    }

    override fun getUsername(): String {
        return user.username
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    override fun isCredentialsNonExpired(): Boolean { // TODO Auto-generated method stub
        return true
    }

    override fun isEnabled(): Boolean { // TODO Auto-generated method stub
        return true
    }

    companion object {
        private const val serialVersionUID = 1256711395932122675L
    }

    init {
        this.user = user
    }

}