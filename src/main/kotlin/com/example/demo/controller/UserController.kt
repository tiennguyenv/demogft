package com.example.demo.controller

import com.example.demo.model.Charge
import com.example.demo.model.UserEntity
import com.example.demo.service.UserService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/users")
class UserController (private var userService: UserService) {
    @Operation(summary = "List All Users", description = "Return the list Users", tags = ["users"])
    @GetMapping("/listAll")
    @ApiResponses( ApiResponse(responseCode = "200", description = "successful operation"))
    @PreAuthorize("hasRole('ROLE_USER')")
    fun listAllUsers(): ResponseEntity<List<UserEntity>> {
        return ResponseEntity(userService.findAllUsers().map{it}, HttpStatus.OK)
    }
}