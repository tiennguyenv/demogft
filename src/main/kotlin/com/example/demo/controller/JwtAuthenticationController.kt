package com.example.demo.controller

import com.example.demo.jwts.JwtRequest
import com.example.demo.jwts.JwtResponse
import com.example.demo.jwts.JwtTokenUtil
import com.example.demo.service.CustomUserDetails
import com.example.demo.service.CustomUserDetailsService
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.DisabledException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.web.bind.annotation.*
import java.util.stream.Collectors

@RestController
class JwtAuthenticationController(private val authenticationManager: AuthenticationManager, jwtTokenUtil: JwtTokenUtil,
                                  userDetailsService: CustomUserDetailsService) {
    private val jwtTokenUtil: JwtTokenUtil = jwtTokenUtil
    private val userDetailsService: CustomUserDetailsService = userDetailsService

    @PostMapping("/login")
    @Throws(Exception::class)
    fun createAuthenticationToken(@RequestBody authenticationRequest: JwtRequest): ResponseEntity<*> {
        authenticationRequest.username?.let { authenticationRequest.password?.let { it1 -> authenticate(it, it1) } }
        val userDetails: UserDetails? = authenticationRequest.username?.let {
            userDetailsService
                    .loadUserByUsername(it)
        }
        val customerDetails: CustomUserDetails = authenticationRequest.username?.let {
            userDetailsService.loadUserByUsername(it)
        } as CustomUserDetails
        val token: String? = userDetails?.let { jwtTokenUtil.generateToken(it) }
        val roles = userDetails?.authorities?.stream()
                ?.map { item: GrantedAuthority? -> item!!.authority }
                ?.collect(Collectors.toList())
        val response: JwtResponse = JwtResponse(token, userDetails?.username, roles as MutableList<String>, customerDetails.getUser().id)

        return ResponseEntity.ok<Any>(response)
    }

    @Throws(Exception::class)
    private fun authenticate(username: String, password: String) {
        try {
            authenticationManager.authenticate(UsernamePasswordAuthenticationToken(username, password))
        } catch (e: DisabledException) {
            throw Exception("USER_DISABLED", e)
        } catch (e: BadCredentialsException) {
            throw Exception("INVALID_CREDENTIALS", e)
        }
    }

}
