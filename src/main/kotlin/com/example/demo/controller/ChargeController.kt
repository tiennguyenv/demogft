package com.example.demo.controller

import com.example.demo.model.Charge
import com.example.demo.model.UserEntity
import com.example.demo.service.ChargeService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api")
class ChargeController(private var chargeService: ChargeService) {
    @Operation(summary = "Create new charge", description = "", tags = ["charge"])
    @ApiResponses( ApiResponse(responseCode = "201", description = "Charge Created"))
    @PostMapping("/add")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    fun addCharge(@RequestBody body: Charge):ResponseEntity<Charge> {
        var charge = chargeService.createCharge(body)
        return ResponseEntity(charge, HttpStatus.CREATED)
    }

    @Operation(summary = "List All Charges", description = "Return the list Charges", tags = ["charge"])
    @GetMapping("/listAll")
    @ApiResponses( ApiResponse(responseCode = "200", description = "successful operation"))
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    fun listAllCharge(): ResponseEntity<List<Charge>> {
        return ResponseEntity(chargeService.findAll().map{it},HttpStatus.OK)
    }

}