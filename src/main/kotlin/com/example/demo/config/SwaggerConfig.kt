package com.example.demo.config

import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SwaggerConfig {
    @Value("\${open-api.title}")
    private lateinit var title: String

    @Bean
    fun customOpenAPI(): OpenAPI{
        return OpenAPI().components(Components()).info(Info().title(title))
    }
}


