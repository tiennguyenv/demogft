package com.example.demo.repository

import com.example.demo.model.UserEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository : CrudRepository<UserEntity,Long> {
    fun findByUsername(username: String): UserEntity
}