- dùng kotlin + spring boot - Done

- rest api crud 1 entity charge (cardNo, cvv, expiredDate, amount, status) - Done

- dùng swagger làm API spec, API versioning - Done

- JWT để làm authen/author, gắn thêm custom data vào JWT - Done

- dùng kafka xử lý async: khi tạo charge, app nhận charge và mặc định xử lý success trả status = success.
Sau đó trả kết quả về client, đồng thời gửi message vào kafka topic. - Done
Một function khác tự động nhận message từ topic để lưu data vào database làm history (chỉ lưu cardNo, amount, status) - Done

- dùng arvo schema + kafka serialize - Researching Arvo

- db postgres - Halfway in Docker Hub. Moving to Docker-Compose

- triển khai integration test, coverage test với embbeded postgres và flyway - Researching

- encrypt thông tin credential trong properties file (acc, pass vv) - In Progress

- dùng flyways để làm data migration - Done

- tất cả component: app, kafka, zookeeper, database triển trai trong docker compose:

Kafka + Zookeeper Pushed on Docker-Compose